import com.sun.jdi.*;
import com.sun.jdi.Bootstrap;
import com.sun.jdi.ClassType;
import com.sun.jdi.LocalVariable;
import com.sun.jdi.ObjectReference;
import com.sun.jdi.Location;
import com.sun.jdi.StackFrame;
import com.sun.jdi.VMDisconnectedException;
import com.sun.jdi.Value;
import com.sun.jdi.VirtualMachine;
import com.sun.jdi.connect.Connector;
import com.sun.jdi.connect.LaunchingConnector;
import com.sun.jdi.event.BreakpointEvent;
import com.sun.jdi.event.ClassPrepareEvent;
import com.sun.jdi.event.Event;
import com.sun.jdi.event.EventSet;
import com.sun.jdi.event.StepEvent;
import com.sun.jdi.request.BreakpointRequest;
import com.sun.jdi.request.ClassPrepareRequest;
import com.sun.jdi.request.StepRequest;
import java.io.File;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import javax.lang.model.type.ArrayType;
import java.lang.Class;

//================================================
// Java Analyzer Class
//================================================

public class Analyzer {
	private Class debugClass;
	private static int historyID;

	// ================================================
	// getter and setter
	// ================================================

	public void setDebugClass(Class targetClass) {
		this.debugClass = targetClass;
	}

	public Class getDebugClass() {
		return debugClass;
	}

	// ================================================
	// Analyzer main
	// javafileフォルダ内のJavaファイルすべてを解析対象とする．
	// その中でのMainクラスをコマンドライン引数で指定する．
	// 誤っていたり，存在しない場合はエラーとして処理する．
	// ================================================
	public static void main(String[] args) throws Exception {
		Analyzer analyzerClass = new Analyzer();

		File dir = new File("javafile"); // Fileクラスのオブジェクトを生成し対象のディレクトリを指定
		File[] list = dir.listFiles(); // listFilesを使用してファイル一覧を取得
		ArrayList<String> debugClassNameAndJavaList = new ArrayList<>(); // DebugClassName
		ArrayList<String> debugClassNameList = new ArrayList<>(); // DebugClassName
		for (int i = 0; i < list.length; i++) {
			if (list[i].getName().matches(".+java")) { // javafileフォルダー内の.javaファイルのみ検出
				String tmp = list[i].getName();
				String[] splitTmp = tmp.split(".java");
				debugClassNameAndJavaList.add(tmp);
				debugClassNameList.add(splitTmp[0]);
				System.out.println(Arrays.deepToString(splitTmp)); // ファイル名のみ
			}
		}

		Class mainDebugClassName = Class.forName("javafile.Method");
		analyzerClass.setDebugClass(mainDebugClassName); // Debug Class Name
		// System.err.println("test Class = " + Test.class);
		// analyzerClass.setDebugClass(Test.class); // Debug Class Name
		historyID = 2;

		/*
		 * Prepare connector, set class to debug & launch VM.
		 */
		LaunchingConnector launchingConnector = Bootstrap.virtualMachineManager().defaultConnector();
		Map<String, Connector.Argument> env = launchingConnector.defaultArguments();
		env.get("main").setValue(analyzerClass.getDebugClass().getName());
		VirtualMachine vm = launchingConnector.launch(env);

		/*
		 * Request VM to trigger event when HelloWorld class is prepared.
		 */
		ClassPrepareRequest classPrepareRequest = vm.eventRequestManager().createClassPrepareRequest();
		classPrepareRequest.addClassFilter(analyzerClass.getDebugClass().getName());
		classPrepareRequest.enable();

		EventSet eventSet = null;

		int i = 0;
		Boolean isSkip = true;
		int statementID = 1;
		try {
			while ((eventSet = vm.eventQueue().remove(100)) != null) {
				for (Event event : eventSet) {
					isSkip = true;
					for (String check : debugClassNameList) {
						if (event.toString().matches(".*" + check + ".*"))
							isSkip = false;
					}

					if (!isSkip)
						System.err.println("#" + i + "# " + event.toString());
					i++;
					boolean isDebugClassName = true;
					/*
					 * If this is ClassPrepareEvent, then set breakpoint
					 */
					if (event instanceof ClassPrepareEvent) {
						ClassPrepareEvent evt = (ClassPrepareEvent) event;
						ClassType classType = (ClassType) evt.referenceType();
						Method methodMain = classType.concreteMethodByName("main", "([Ljava/lang/String;)V");

						Location location = methodMain.location();
						BreakpointRequest bpReq = vm.eventRequestManager().createBreakpointRequest(location);
						bpReq.enable();
					}

					/*
					 * If this is BreakpointEvent, then read & print variables.
					 */
					if (event instanceof BreakpointEvent) {

						// disable the breakpoint event
						event.request().disable();

						try {
							// Get values of all variables that are visible and print
							StackFrame stackFrame = ((BreakpointEvent) event).thread().frame(0);
							Location loc = stackFrame.location();

							// 最初の情報
							statementID = loc.lineNumber();

							List<LocalVariable> abc = stackFrame.visibleVariables();
							System.err.println("Visible Variables N: " + abc.size());

							Map<LocalVariable, Value> visibleVariables = (Map<LocalVariable, Value>) stackFrame
									.getValues(stackFrame.visibleVariables());
							System.err.println("Local Variables =");
							for (Map.Entry<LocalVariable, Value> entry : visibleVariables.entrySet()) {
								System.err.println(" " + entry.getKey().name() + " = " + entry.getValue());
							}

						} catch (Exception e) {
							e.printStackTrace();
						}

						// Request for stepping through lines after this breakpoint
						StepRequest stepRequest;
						if (isDebugClassName) {
							stepRequest = event.virtualMachine().eventRequestManager()
									.createStepRequest(((BreakpointEvent) event).thread(), StepRequest.STEP_LINE, StepRequest.STEP_INTO);
						} else {
							stepRequest = event.virtualMachine().eventRequestManager()
									.createStepRequest(((BreakpointEvent) event).thread(), StepRequest.STEP_LINE, StepRequest.STEP_OUT);
						}
						stepRequest.enable();
					}

					/*
					 * If this is StepEvent, then read & print variables.
					 */
					if (event instanceof StepEvent) {
						try {
							if (!isSkip) {
								// Get values of all variables that are visible and print
								StackFrame stackFrame = ((StepEvent) event).thread().frame(0);
								Location loc = stackFrame.location();

								System.err.println("historyID = " + historyID);
								System.err.println("statementID = " + statementID);
								statementID = loc.lineNumber();

								System.err.println("StackFrameCount:" + ((StepEvent) event).thread().frameCount());

								List<LocalVariable> abc = stackFrame.visibleVariables();
								System.err.println("Visible Variables N: " + abc.size());

								ObjectReference obj = stackFrame.thisObject();

								ReferenceType stepReference = loc.declaringType();
								System.err.println("HashCode:" + stepReference.hashCode());
								System.err.println("---Field Variables at this step---");
								for (Field fieldData : stepReference.fields()) {
									String finalTypeName = loc.sourceName().replaceAll(".java", "") + "_";
									finalTypeName += (fieldData.isFinal()) ? "FINAL_" : "";
									if (fieldData.isStatic()) {
										finalTypeName += "STATIC_";
										System.err.println(finalTypeName + fieldData.typeName().replaceAll("java.lang.", "") + ","
												+ fieldData.name() + "," + stepReference.getValue(fieldData));
									} else {
										finalTypeName += obj.uniqueID() + "_";
										System.err.println(finalTypeName + fieldData.typeName().replaceAll("java.lang.", "") + ","
												+ fieldData.name() + "," + obj.getValue(fieldData));
									}
								}
								System.err.println("---End Field Variables---");

								System.err.println(loc.sourceName());
								System.err.println("sourcePath = " + loc.sourcePath());
								if (debugClassNameAndJavaList.contains(loc.sourceName())) {
									historyID++;
									isDebugClassName = true;
									Map<LocalVariable, Value> visibleVariables = (Map<LocalVariable, Value>) stackFrame
											.getValues(stackFrame.visibleVariables());
									System.err.println("---Local Variables at this step---");
									String methodTmpName = loc.method().toString();
									String[] methodTmpName2 = methodTmpName.split("\\.");
									String[] methodName = methodTmpName2[2].split("\\(");
									System.err.println(methodName[0] + "," + loc.method().returnType()); // 現在実行中のメソッド名, メソッドの返り値の型
									for (Map.Entry<LocalVariable, Value> entry : visibleVariables.entrySet()) {
										String isArg = (entry.getKey().isArgument()) ? "T" : "F";
										if (entry.getKey().type().name().contains("[]")) {
											// instance of int[length] (id=xx) を空白で分割
											// instance of java.lang.String[3] (id=95)
											// tablevalue[2] にデータが入っている
											// ただし，Stringのみ別の処理が必要？
											String[] tableValue = entry.getValue().toString().split(" ", 0);
											if (entry.getKey().type().name().replaceAll("java.lang.", "").equals("String[]")) {
												tableValue[2] = tableValue[2].replaceAll("java.lang.", "");
											}
											// xx[length] -> length へ変換する
											String[] tmp = tableValue[2].split("\\[");
											String[] tmp2 = tmp[1].split("\\]");
											tableValue[2] = tmp2[0];
											// tableValue[2] には length が入っている
											System.err.println(entry.getKey().type().name().replaceAll("java.lang.", "") + ","
													+ entry.getKey().name() + "[]," + tableValue[2] + "," + isArg);
											ArrayReference arrayData = (ArrayReference) entry.getValue();
											for (int p = 0; p < arrayData.length(); p++) {
												System.err.println(entry.getKey().type().name().replaceAll("java.lang.", "") + ","
														+ entry.getKey().name() + "[" + p + "]," + arrayData.getValue(p) + "," + isArg);
											}
										} else {
											System.err.println(entry.getKey().type().name().replaceAll("java.lang.", "") + ","
													+ entry.getKey().name() + "," + entry.getValue() + "," + isArg);
										}
									}
									System.err.println("---End Local Variables---");
								} else {
									isDebugClassName = false;
								}
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					vm.resume();
				}
			}
		} catch (VMDisconnectedException e) {
			System.err.println("VM is now disconnected.");
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			System.err.println("finalyStatementID = " + statementID);
			InputStreamReader reader = new InputStreamReader(vm.process().getInputStream());
			OutputStreamWriter writer = new OutputStreamWriter(System.err);
			char[] buf = new char[1024];

			reader.read(buf);
			writer.write(buf);
			writer.flush();
		}
	}
}