# メモリスト

小暮研M2(2021年時点)

作成者：鈴木友佑

作成時のメモを残しています．

## 2021/5/26時点
* History.js生成部分の実装
	- func callをどうするか
	- 変数のスコープをどうするか -> いったんは無し，あるメソッドリターン->メモリから消える
	- includeをimportへ(いらないかも)

* インスタンス　C言語でのポインターのアドレス


1. まずはmain文内で完結する，ただ計算するだけのプログラムの描画ができるようにする
2. 同一クラス内でメソッド呼び出しが描画できるようにする
3. 他クラスのインスタンスを生成し，そのインスタンスのメソッドが描画できるようにする


## 【メモ】
新規Javaファイルを解析する場合の手順は以下のとおり
1. javafile内に新規Javaクラス(XXX.java)を作成
2. Analyzer.javaの
Class mainDebugClassName = Class.forName("javafile.XXX");
を変更する．
3. createHistoryJSONFile.javaのoutputHistoryJsonFileNameを関連する名前に変更
例：history-XXXjava.js
4. createHTMLFile.javaのoutputHTMLFileNameを関連する名前に変更する
例：source-XXXjava.html
5. run.batのコピー元，コピー先の名前を3,4で決めた名前に変更する．
例：echo ===== copy html file =====
copy /Y source-XXXjava.html C:\Users\arrow\Documents\TEDViT\work-tedvit\dist\assets\newsystem\html\source-XXXjava.html
echo ===== copy history-js file =====
copy /Y history-XXXjava.js C:\Users\arrow\Documents\TEDViT\work-tedvit\dist\assets\newsystem\js\history-XXXjava.js

6. (おまけ)sample.htmlのリストに上記のTEDViTへのリンクを追加する
例：`<li> <a href="tedvit.html?html=XXXjava&history=XXXjava">XXXの確認</a>`

