package javafile;

import javafile.InstanceCheck;

public class InstanceCheck {
	public static void main(String[] args) {
		Cat tama = new Cat();
		tama.name = "neko";
		tama.age = 5;
	}
}

class Cat {
	String name;
	int age;
}