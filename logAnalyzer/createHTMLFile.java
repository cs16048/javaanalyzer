package logAnalyzer;

import java.io.*;

public class createHTMLFile {
	public static String outputHTML;

	// =========================================
	// main
	// args[0] : 出力ファイル名
	// -> nullの場合は source-test.html と出力
	// =========================================
	public static void main(String[] args) {
		// 出力するHTMLファイルの名前の決定
		String outputHTMLFileName = "./logAnalyzer/html/source-";
		if (args.length > 0) {
			outputHTMLFileName += args[0] + ".html";
		} else {
			outputHTMLFileName += "methodjava.html";
		}
		// 出力ファイルをあらかじめ作成する
		setHTML(outputHTMLFileName);

		checkLine("sourcePath = javafile/Method.java");

		// 作成したHTMLファイルに書き込み
		writeHTML(outputHTMLFileName);
	}

	// =========================================
	// setHTML
	// HTMLファイルの作成機構
	// outputHTMLFileName : 出力するファイルの名前
	// =========================================
	public static void setHTML(String outputHTMLFileName) {

		File newfile = new File(outputHTMLFileName);
		try {
			if (newfile.createNewFile()) {
				System.out.println("sucess create source HTML File");
			} else {
				System.out.println("Failed create source HTML File");
			}
		} catch (IOException e) {
			System.out.println(e);
		}
		outputHTML = "<pre><code>";
	}

	// =========================================
	// writeHTML
	// setHTMLで作成したファイルに文字列を書き込む
	// outputHTMLFileName : 出力するファイルの名前
	// =========================================
	public static void writeHTML(String outputHTMLFileName) {
		outputHTML += "</code></pre>";
		try {
			File file = new File(outputHTMLFileName);
			FileWriter filewriter = new FileWriter(file);

			// 作成したファイルに書き込み
			filewriter.write(outputHTML);

			// ファイルを閉じる
			filewriter.close();
		} catch (IOException e) {
			System.out.println(e);
		}
	}

	public static void checkLine(String line) {
		// if (isWriteFile) {
		// String[] lineNum = line.split(" = ", 0);
		// System.out.println("Line Num = " + lineNum[1]);
		// // outputHTML += "Line Num = " + lineNum[1] + "\n";

		// isWriteFile = false;
		// return;
		// }

		if (line.matches(".*sourcePath.*")) {
			String[] path = line.split(" = ", 0);
			if (path[1].matches(".*javafile.*")) {
				// String fileName = "../" + path[1];
				String fileName = path[1];
				int statementID = 0;
				try (BufferedReader in = new BufferedReader(new FileReader(new File(fileName)))) {
					String lineString;
					while ((lineString = in.readLine()) != null) {
						statementID++;
						if (lineString != "") {
							outputHTML += "<span id=statement" + statementID + ">" + lineString + "</span>\n";
						}
					}
				} catch (FileNotFoundException e) {
					e.printStackTrace();
					System.exit(-1);
				} catch (IOException e) {
					e.printStackTrace();
					System.exit(-1);
				}
			}
		}
	}
}
