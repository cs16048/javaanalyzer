import java.io.*;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Stack;

class variablesTableClass {
	String variableType;
	String variableName;
	String variableValue;

	// ---------------
	// constructor
	// ---------------
	variablesTableClass(String type, String name, String value) {
		this.variableType = type;
		this.variableName = name;
		this.variableValue = value;
	}
}

public class createHistoryJSONFile {
	public static String logPath = "../log.txt"; // 解析対象のファイルパス
	public static String classValueKey = "CLASSVALIABLETABLEKEY";

	public enum STATE {
		NONE, CLASSVALUE, LOCALVALUE, LOCALMETHODNAME,
	}

	public static String outputHistoryJson;
	public static String outputCSVRule;

	// =========================================
	// main
	// args[0] : 出力ファイル名
	// -> nullの場合は source-test.html と出力
	// =========================================
	public static void main(String[] args) {
		// 出力するHistory.jsファイル名の決定
		String outputHistoryJsonFileName = "./historyjs/history-";
		// 出力するルールファイル名の決定
		String outputCSVFileName = "./rule/rule-";
		if (args.length > 0) {
			outputHistoryJsonFileName += args[0] + ".js";
			outputCSVFileName += args[0] + ".csv";
		} else {
			outputHistoryJsonFileName += "methodjava.js";
			outputCSVFileName += "methodjava.csv";
		}

		// 出力ファイルをあらかじめ作成する
		setJSON(outputHistoryJsonFileName);

		outputCSVRule = "type,rulename,conditional,op,objName,objType,val,xpoint,ypoint,linecolor,backcolor,charcolor,sobj,eobj,skind,ekind,autouppdate,lineweight,string,align,baseline,tobject,balstate,balstyle,tablerow,tablecolumn,tablelayout,tablespace,tablelabel,format,abc"
				+ "\n\n";
		outputCSVRule += "#変数を生成する.\n";

		// 解析処理
		readLog();

		// 作成したHistory.jsファイルに書き込み
		writeJSON(outputHistoryJsonFileName);

		// 作成したルールファイルの書き込み
		writeCSVRule(outputCSVFileName);
	}

	// =========================================
	// writeCSVRule
	// CSVファイル(Ruleファイル)を出力する処理
	// =========================================
	public static void writeCSVRule(String writeFileName) {
		try {
			PrintWriter pointWriter = new PrintWriter(
					new BufferedWriter(new OutputStreamWriter(new FileOutputStream(writeFileName), "UTF-8")));

			// 作成したファイルに書き込み
			pointWriter.print(outputCSVRule);

			// ファイルを閉じる
			pointWriter.close();
		} catch (IOException e) {
			System.out.println(e);
		}
	}

	// =========================================
	// setJSON
	// History.jsファイルの作成機構
	// outputHistoryJsonFileName : 出力するファイルの名前
	// =========================================
	public static void setJSON(String outputHistoryJsonFileName) {

		File newfile = new File(outputHistoryJsonFileName);
		try {
			if (newfile.createNewFile()) {
				System.out.println("sucess create source History.js File");
			} else {
				System.out.println("Failed create source History.js File");
			}
		} catch (IOException e) {
			System.out.println(e);
		}
		outputHistoryJson = "[\n{\"hid\": 0,\"op\": \"start\"},\n";
		outputHistoryJson += "{\"hid\": 1, \"sid\" : 1, \"comment\": \"\", \"op\" : \"declaration_function\", \"name\" : \"main\", \"type\" : \"void\"}, \n";
	}

	// =========================================
	// writeJSON
	// setJSONで作成したファイルに文字列を書き込む
	// outputHistoryJsonFileName : 出力するファイルの名前
	// =========================================
	public static void writeJSON(String outputHistoryJsonFileName) {
		outputHistoryJson += "]";
		try {
			File file = new File(outputHistoryJsonFileName);
			FileWriter filewriter = new FileWriter(file);

			// 作成したファイルに書き込み
			filewriter.write(outputHistoryJson);

			// ファイルを閉じる
			filewriter.close();
		} catch (IOException e) {
			System.out.println(e);
		}
	}

	/// <summary>
	/// 1行分JSONデータを作成する
	/// </summary>
	/// <param name="data">出力用データ</param>
	/// <param name="isEnd">最終行かどうか</param>
	public static void setOutputData(HistoryData data, boolean isEnd) {
		outputHistoryJson += "{";
		outputHistoryJson += "\"hid\": " + data.hid + ", ";
		outputHistoryJson += "\"sid\":" + data.sid + ", ";
		outputHistoryJson += "\"comment\": \"\", ";
		outputHistoryJson += "\"op\":\"" + data.op + "\", ";
		outputHistoryJson += "\"name\":\"" + data.name + "\", ";
		outputHistoryJson += "\"type\":\"" + data.type + "\", ";
		outputHistoryJson += "\"scope\":" + data.scope + ", ";
		outputHistoryJson += "\"value\":";
		outputHistoryJson += (data.value == "") ? "\"\", " : data.value + ", ";
		outputHistoryJson += "\"address\":" + data.address + ", ";
		outputHistoryJson += "\"leftexp\":\"" + data.leftexp + "\", ";
		outputHistoryJson += "\"rightexp\":\"" + data.rightexp + "\", ";
		outputHistoryJson += "\"arrayValue\":";
		outputHistoryJson += (data.arrayValue == "") ? "\"\", " : data.arrayValue + ", ";
		outputHistoryJson += "\"isArray\":" + data.isArray + ", ";
		outputHistoryJson += "\"arraySize\":" + data.arraySize + ", ";
		outputHistoryJson += "\"unitMemoriSize\":" + data.unitMemoriSize;
		outputHistoryJson += (isEnd) ? "}\n" : "},\n";
	}

	// デフォルト引数用
	public static void setOutputData(HistoryData data) {
		setOutputData(data, false);
	}

	// =========================================
	// setRuleData
	// 1行分のルールデータを出力する
	// =========================================
	public static void setRuleData(RuleData data) {
		outputCSVRule += data.type + ",";
		outputCSVRule += data.rulename + ",";
		outputCSVRule += data.conditional + ",";
		outputCSVRule += data.op + ",";
		outputCSVRule += data.objName + ",";
		outputCSVRule += data.objType + ",";
		outputCSVRule += data.val + ",";
		outputCSVRule += data.xpoint + ",";
		outputCSVRule += data.ypoint + ",";
		outputCSVRule += data.linecolor + ",";
		outputCSVRule += data.backcolor + ",";
		outputCSVRule += data.charcolor + ",";
		outputCSVRule += data.sobj + ",";
		outputCSVRule += data.eobj + ",";
		outputCSVRule += data.skind + ",";
		outputCSVRule += data.ekind + ",";
		outputCSVRule += data.autouppdate + ",";
		outputCSVRule += data.lineweight + ",";
		outputCSVRule += data.string + ",";
		outputCSVRule += data.align + ",";
		outputCSVRule += data.baseline + ",";
		outputCSVRule += data.tobject + ",";
		outputCSVRule += data.balstate + ",";
		outputCSVRule += data.balstyle + ",";
		outputCSVRule += data.tablerow + ",";
		outputCSVRule += data.tablecolumn + ",";
		outputCSVRule += data.tablelayout + ",";
		outputCSVRule += data.tablespace + ",";
		outputCSVRule += data.tablelabel + ",";
		outputCSVRule += data.format + ",";
		outputCSVRule += data.abc;
		outputCSVRule += "\n";
	}

	// =========================================
	// readLog
	// Analyzerが出力したlog.txtを解析し，
	// 変数などの情報からhistory.jsを作っていくメソッド
	// 1行ずつ解析を行う
	// =========================================
	public static void readLog() {
		STATE nowState = STATE.NONE;
		HashMap<String, ArrayList<variablesTableClass>> fVarTable = new HashMap<String, ArrayList<variablesTableClass>>();
		ArrayList<variablesTableClass> fVarTmp = new ArrayList<variablesTableClass>();
		Stack<HashMap<String, ArrayList<variablesTableClass>>> lVarStack = new Stack<HashMap<String, ArrayList<variablesTableClass>>>();
		HashMap<String, ArrayList<variablesTableClass>> lVarTable = new HashMap<String, ArrayList<variablesTableClass>>();
		ArrayList<variablesTableClass> lVarTmp = new ArrayList<variablesTableClass>();

		// outputするJSONデータのクラス
		HistoryData historyData = new HistoryData();
		int stackPoint = 1;
		int historyID = 1;
		int statementID = 1;
		String methodName = "";
		boolean isPop = false;
		// 新規メソッドが実行されたかどうか
		boolean isNewMethod = false;
		// 新規メソッドが実行された場合
		// ステートメントIDは実行前の値
		// ローカル変数は実行後の値
		// が表示されているため
		// 次のステップに値を保持しておく必要がある．
		ArrayList<HistoryData> newMethodLocalValues = new ArrayList<HistoryData>();

		// Ruleファイルに出力する変数を管理するリスト
		// key : 変数名
		// value : RuleData
		HashMap<String, RuleData> ruleVariableList = new HashMap<String, RuleData>();

		int arrayLength = 0;
		int currentArrayCount = 0;

		try (BufferedReader in = new BufferedReader(new FileReader(new File(logPath)))) {
			// line にログファイルの1行分の文字列を格納する
			String line;
			/*------- 文字列解析 -------*/
			while ((line = in.readLine()) != null) {
				if (line.matches(".*StackFrameCount.*")) {
					// StackFrameCountの値を取り出す
					String[] data = line.split(":", 0); // 0:string, 1:StackFrameCount
					int tmpPoint = Integer.parseInt(data[1]);
					System.out.println("***tmpPoint = " + tmpPoint + ", stackPoint = " + stackPoint);
					if (stackPoint == tmpPoint) { // 変化なし
						isPop = false;
						continue;
					}
					if (stackPoint < tmpPoint) { // PUSHする必要がある
						isPop = false;
						stackPoint = tmpPoint;
						System.out.println("=====PUSH=====");
						lVarStack.push(lVarTable);
						for (int k = 0; k < lVarTable.get(methodName).size(); k++) { // table内に変数がある->値の確認，変わっていたら代入とみなす
							System.out.println(lVarTable.get(methodName).get(k).variableName + ", value = "
									+ lVarTable.get(methodName).get(k).variableValue);
						}
						lVarTable = new HashMap<String, ArrayList<variablesTableClass>>();
						continue;
					}
					if (stackPoint > tmpPoint) {
						// POPする必要あり:returnの検知
						stackPoint = tmpPoint;
						isPop = true;
						System.out.println("===BEFORE===");
						for (String key : lVarTable.keySet()) {
							for (int q = 0; q < lVarTable.get(key).size(); q++) {
								System.out.println(
										lVarTable.get(key).get(q).variableName + ", value = " + lVarTable.get(key).get(q).variableValue);
							}
						}
						System.out.println("=====POP=====");
						lVarTable = lVarStack.pop();
						for (String key : lVarTable.keySet()) {
							System.out.println("=====STACK DATA====");
							for (int q = 0; q < lVarTable.get(key).size(); q++) {
								System.out.println(
										lVarTable.get(key).get(q).variableName + ", value = " + lVarTable.get(key).get(q).variableValue);
							}

						}
						// return文の実行
						historyData.op = "return";
						// lVarStack.pop();
						continue;
					}
				}
				// HistoryIDを設定
				if (line.matches(".*historyID.*")) {
					// ------------ すべての始まりはここから -------------------
					historyData = new HistoryData();
					historyID++;
					historyData.hid = historyID;
				}
				// StatementIDを設定
				if (line.matches(".*statementID.*")) {
					String[] statementIDs = line.split(" = ", 0);
					statementID = Integer.parseInt(statementIDs[1]);
					// 次のステップに変わったあと
					// 前のステップが新規メソッドを実行していたら積んだリストを出力する
					// 終了後はfalseに設定しておく
					if (isNewMethod) {
						isNewMethod = false;
						for (HistoryData hData : newMethodLocalValues) {
							hData.hid = historyID;
							historyID++;
							hData.sid = statementID - 1;
							setOutputData(hData);
						}
						historyData.hid = historyID;
					}
					historyData.sid = statementID;
				}
				// フィールド変数(クラス変数)の出力開始合図
				if (line.matches(".*---Field Variables at this step---.*")) {
					nowState = STATE.CLASSVALUE;
					if (!fVarTable.containsKey(classValueKey)) {
						fVarTmp = new ArrayList<variablesTableClass>();
					}
					continue;
				}
				// フィールド変数(クラス変数)の出力終了合図
				if (line.matches(".*---End Field Variables---.*")) {
					nowState = STATE.NONE;
					// switch文で作ったtmpをtableに登録
					if (!fVarTable.containsKey(classValueKey)) {
						fVarTable.put(classValueKey, fVarTmp);
					}
					continue;
				}
				// ローカル変数の出力開始合図
				if (line.matches(".*---Local Variables at this step---.*")) {
					nowState = STATE.LOCALMETHODNAME;
					methodName = "";
					continue;
				}
				// ローカル変数の出力終了合図＆1ステップ修了合図
				if (line.matches(".*---End Local Variables---.*")) {
					nowState = STATE.NONE;
					if (!isPop && !lVarTable.containsKey(methodName)) {
						lVarTable.put(methodName, lVarTmp);
					}

					// ------------- ここで1ステップが終わる ----------------
					if (!isNewMethod)
						setOutputData(historyData);
					continue;
				}
				// 全ステップ終了合図
				if (line.matches(".*finalyStatementID.*")) {
					// ------------ ここですべてが終わる -------------------
					String[] finalyIDs = line.split(" = ", 0);
					statementID = Integer.parseInt(finalyIDs[1]);
					historyData = new HistoryData();
					historyData.hid = historyID + 1;
					historyData.sid = statementID;
					setOutputData(historyData, true);
				}

				// 各状態で処理を分ける
				switch (nowState) {
					case CLASSVALUE: // クラス変数の登録
						// length == 3となるようにログを作る
						String[] data = line.split(",", 0); // 0:type, 1:name, 2:value

						// keyが存在しない -> 最初の変数の登録
						// keyが存在する -> 変数の値の確認
						if (!fVarTable.containsKey(classValueKey)) {
							variablesTableClass addData = new variablesTableClass(data[0], data[1], data[2]);
							fVarTmp.add(addData);
						} else {
							for (int k = 0; k < fVarTable.get(classValueKey).size(); k++) {
								// table内に変数がある->値の確認，変わっていたら代入とみなす
								// table内に変数がない->変数宣言
								if (fVarTable.get(classValueKey).get(k).variableName.equals(data[1])) {
									if (!fVarTable.get(classValueKey).get(k).variableValue.equals(data[2])) {
										/*---- クラス変数の代入 -----*/
										fVarTable.get(classValueKey).get(k).variableValue = data[2];
										// System.out.println("valueName = " +
										// fVarTable.get(classValueKey).get(k).variableName + ", change = "
										// + fVarTable.get(classValueKey).get(k).variableValue);

										// 変数代入 変数名 = 値;であるはず
										historyData.leftexp = data[1];
										historyData.rightexp = data[2];
										// outputHistoryJson += "\"leftexp\" : \"" + data[1] + "\", \"rightexp\" : \"" +
										// data[2] + "\", ";
									}
								} else {
									// global変数の宣言とみなす．(global変数の宣言ってある？)
								}
							}
						}
						break;
					case LOCALMETHODNAME: // ローカル変数の1行目はメソッド名と戻り値の型が書かれている
						data = line.split(",", 0); // 0:name, 1:return value
						methodName = data[0] + "_" + data[1]; // キーは「メソッド名_返り値の型」として定義

						// POPしてたらその情報をlVarTableに格納している．
						// POPしていない，かつ，lVarTableにキーが存在しない->新規メソッド実行している
						if (!isPop && !lVarTable.containsKey(methodName)) {
							lVarTmp = new ArrayList<variablesTableClass>();

							// 新規メソッドの実行
							// ただしmain文は除く
							if (!data[0].matches(".*main.*")) {
								isNewMethod = true;
								newMethodLocalValues = new ArrayList<HistoryData>();
								historyData.op = "declaration_function";
								historyData.name = data[0];
								historyData.type = data[1];
								// 呼ばれた瞬間はその地点にいるのでそのまま1行出してOK
								setOutputData(historyData);
								// 次の変数用に初期化
								historyData = new HistoryData();
							}

						}

						nowState = STATE.LOCALVALUE;
						break;
					case LOCALVALUE: // 2行目からローカル変数情報が表示されている
						data = line.split(",", 0); // 0:type,1:name, 2:value(配列の場合はLength), 3:その変数が引数かどうか(True:T，False:F)

						// keyが存在しない -> 最初の変数の登録
						// keyが存在する -> 変数の値の確認
						if (!lVarTable.containsKey(methodName)) {
							/* ---- lVarTableにローカル変数を初期登録 ---- */
							variablesTableClass addData = new variablesTableClass(data[0], data[1], data[2]);
							lVarTmp.add(addData);

							// 変数新規登録
							// ただしargsは除く
							if (!data[1].matches(".*args\\[\\].*")) {
								// 型名に[]がついていたら配列と判断する．
								if (data[0].matches(".*\\[\\]*")) {
									// xxx[]は配列の変数名を意味する．
									if (data[1].matches(".*\\[\\].*")) {
										// data[0].replaceAll("\\[\\]", "") は配列型名の[]の2文字を空文字に置換する処理
										// data[1].replaceAll("\\[\\]", "") は配列変数名の[]の2文字を空文字に置換する処理
										historyData.op = "declaration";
										historyData.arraySize = Integer.parseInt(data[2]);
										historyData.type = data[0].replaceAll("\\[\\]", "");
										historyData.isArray = 1;
										historyData.name = data[1].replaceAll("\\[\\]", "");
										historyData.scope = -1;
										historyData.address = historyID;
										historyData.unitMemoriSize = 4;
										historyData.arrayValue = "[";

										arrayLength = Integer.parseInt(data[2]);
									} else {
										if (currentArrayCount < arrayLength) {
											historyData.arrayValue += data[2] + ",";
											currentArrayCount++;
										} else {
											historyData.arrayValue += data[2] + "]";
											currentArrayCount = 0;
										}
									}
								} else {
									historyData.op = "declaration";
									historyData.name = data[1];
									historyData.type = data[0];
									// instanceなら""をつける
									historyData.value = (data[2].matches(".*instance.*")) ? "\"" + data[2] + "\"" : data[2];
									historyData.address = historyID;
									historyData.scope = 2;
								}

								// 新規メソッドが実行されている
								// かつ
								// 配列の場合はすべての要素を追加し終わっている
								// ならばリストに追加する
								if (isNewMethod && currentArrayCount == 0) {
									// いまの変数データを登録
									newMethodLocalValues.add(historyData);
									// 次の変数用に初期化
									historyData = new HistoryData();
								}
							}
						} else {
							boolean isVariableDeclaration = true;
							for (int k = 0; k < lVarTable.get(methodName).size(); k++) {
								// table内に変数がある->値の確認，変わっていたら代入とみなす
								if (lVarTable.get(methodName).get(k).variableName.equals(data[1])) {
									if (!lVarTable.get(methodName).get(k).variableValue.equals(data[2])) {
										/* ---- ローカル変数の代入 ---- */
										lVarTable.get(methodName).get(k).variableValue = data[2];
										// System.out.println("localValue = " +
										// lVarTable.get(methodName).get(k).variableName + ", change = "
										// + lVarTable.get(methodName).get(k).variableValue);

										// 変数代入 変数名 = 値;であるはず
										historyData.op = "assignment_expression";
										historyData.leftexp = data[1];
										historyData.rightexp = data[2];
										historyData.value = data[2];
										historyData.address = historyID;
									}
									isVariableDeclaration = false;
									break;
								}
							} // table内に変数がない->変数宣言
							if (isVariableDeclaration) {
								/* ---- ローカル変数の宣言 ---- */
								// System.out.println("key = " + methodName + ",data[1] = " + data[1]);
								variablesTableClass addData = new variablesTableClass(data[0], data[1], data[2]);
								lVarTmp.add(addData);

								// 変数宣言
								// ただしargsは除く
								if (!data[1].matches(".*args\\[\\].*")) {
									// 型名に[]がついていたら配列と判断する．
									if (data[0].matches(".*\\[\\]*")) {
										// xxx[]は配列の変数名を意味する．
										if (data[1].matches(".*\\[\\].*")) {
											// data[0].replaceAll("\\[\\]", "") は配列型名の[]の2文字を空文字に置換する処理
											// data[1].replaceAll("\\[\\]", "") は配列変数名の[]の2文字を空文字に置換する処理
											historyData.op = "declaration";
											historyData.arraySize = Integer.parseInt(data[2]);
											historyData.type = data[0].replaceAll("\\[\\]", "");
											historyData.isArray = 1;
											historyData.name = data[1].replaceAll("\\[\\]", "");
											historyData.scope = 2;
											historyData.address = historyID + 100;
											historyData.unitMemoriSize = 4;
											historyData.arrayValue = "[";
											arrayLength = Integer.parseInt(data[2]);
										} else {
											if (currentArrayCount < arrayLength - 1) {
												historyData.arrayValue += data[2] + ",";
												currentArrayCount++;
											} else {
												historyData.arrayValue += data[2] + "]";
												currentArrayCount = 0;
											}
										}
									} else {
										historyData.op = "declaration";
										historyData.name = data[1];
										historyData.type = data[0];
										historyData.value = (data[2].matches(".*instance.*")) ? "\"" + data[2] + "\"" : data[2];
										historyData.address = historyID;
										historyData.scope = 2;
									}
								}
							}
						}
						break;
					default:
						break;
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.exit(-1); // 0 以外は異常終了
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}
}
