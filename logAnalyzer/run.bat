cd ..

javac -encoding UTF-8 -g *.java > AnalyzerJavacLog.txt 2>&1

cd javafile

javac -encoding UTF-8 -g *.java > javaFileJavacLog.txt 2>&1

cd ..

java Analyzer > log.txt 2>&1

echo ===== create html file =====

javac -encoding UTF-8 logAnalyzer/createHTMLFile.java

java logAnalyzer/createHTMLFile > logAnalyzer/html_log.txt 2>&1

cd logAnalyzer

echo ===== create history-js file =====

javac -encoding UTF-8 createHistoryJSONFile.java

java createHistoryJSONFile > historyJSON_log.txt 2>&1

echo ===== copy html file =====
copy /Y html\source-methodjava.html C:\Users\arrow\Documents\TEDViT\work-tedvit\dist\assets\newsystem\html\source-methodjava.html
echo ===== copy history-js file =====
copy /Y historyjs\history-methodjava.js C:\Users\arrow\Documents\TEDViT\work-tedvit\dist\assets\newsystem\js\history-methodjava.js
pause