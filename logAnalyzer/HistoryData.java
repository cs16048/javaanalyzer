public class HistoryData {
	public int hid;
	public int sid;
	public String comment;
	public String op;
	public String name;
	public String type;
	public int scope;
	public Object value;
	public int address;
	public String leftexp;
	public String rightexp;
	public String arrayValue;
	public int isArray;
	public int arraySize;
	public int unitMemoriSize;

	public HistoryData() {
		this.hid = 0;
		this.sid = 0;
		this.comment = "";
		this.op = "";
		this.name = "";
		this.type = "";
		this.scope = 0;
		this.value = "";
		this.address = 0;
		this.leftexp = "";
		this.rightexp = "";
		this.arrayValue = "";
		this.isArray = 0;
		this.arraySize = 0;
		this.unitMemoriSize = 0;
	}
}
