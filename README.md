# README #

This repository contains a compiler that analyzes Java files and makes them available for TEDViT.

このリポジトリには、Java ファイルを分析して TEDViT で使用できるようにするコンパイラが含まれています。

## What is this repository for? ##

This repository was created to make TEDViT compatible with Java, an object-oriented language.

このリポジトリはTEDViTをオブジェクト指向言語であるJavaに対応させるために作られました．

## Version List ##

* Java <br>
java version "11.0.8" 2020-07-14 LTS <br>
Java(TM) SE Runtime Environment 18.9 (build 11.0.8+10-LTS) <br>
Java HotSpot(TM) 64-Bit Server VM 18.9 (build 11.0.8+10-LTS, mixed mode) <br>

## Directory structure diagram ##

```DirectoryStructureDiagram
.
├── .gitignore
│     : Files to register as untracked Git files(Git追跡対象外にするファイル)
├── Analyzer.java
│     : Java to parse the target Java file(解析器本体．解析対象を解析する)
├── log.txt
│     : Analysis result of JavaAnalyzer.java(JavaAnalyzerの解析結果)
├── AnalyzerJavacLog.txt
│     : Log data compiled from JavaAnalyzer.java(JavaAnalyzerのjavacの結果)
├── javafile : Folder containing the Java files to be analyzed(解析対象Javaが入ってる)
│   ├── HelloWorld.java
│   ├── Array.java
│   └── Method.java
├── logAnalyzer
│   │  : Examine the log of results analyzed by Analyzer.java(log.txtを解析する)
│   ├── source-xxx.html
│   ├── history-xxx.js
│   ├── createHistoryJSONFile.java
│   ├── createHTMLFile.java
│   ├── HistoryData.java
│   ├── historyJSON_log.txt
│   ├── himl_log.txt
│   └── run.bat
├── README.md
│     : This material(本資料です．)
└── memo.md
     : Notes(実装時のメモです．)
```

### Who do I talk to? ###
